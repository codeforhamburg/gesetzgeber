import os
from bs4 import BeautifulSoup
import requests

#main_page = requests.get('https://www.bundestag.de/parlament/plenum/abstimmung/abstimmung/?id=636').content
sub_page = requests.get('https://www.bundestag.de/apps/na/na/namensliste.form?id=636&ajax=true&limit=5000').content

soup = BeautifulSoup(sub_page , 'html.parser')

#get people and their vote
representatives = soup.select('div[class="bt-slide-content"]')
gathered_data = []

for representative in representatives:
    # get data
    data = {}
    data['name'] = representative.select_one('h3').get_text(strip=True)
    data['abstimmung'] = representative.select_one('p.bt-person-abstimmung').get_text(strip=True)
    data['image_url'] = representative.select_one('img.img-responsive')['data-img-md-normal']    
    data['image_name'] = os.path.basename(data['image_url'])

    # save image if not existing
    if not os.path.isfile(data['image_name']):
        f = open('images/' + data['image_name'],'wb')
        f.write(requests.get(data['image_url']).content)
        f.close()

    gathered_data.append(data)

print(gathered_data)